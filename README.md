# MCS 2050 

This repo is to test your setup for C/C++ projects. As far as code goes, it's no more than a "Hello, World" program - but it serves as an easy target to test setup.

## Cloning the source

After creating an account on the GitLab site, you can clone on the command line via:

```shell
git clone https://gitlab.com/techlab2050/week1.git
```
And enter your credentials. Your username will be the email you signed up to Gitlab with.

Alternatively, if not using the command line, you can paste ```https://gitlab.com/techlab2050/week1.git``` into whatever GUI Git client you're using (e.g. Tortoise Git, SourceTree, or Visual Studio Code), and enter your credentials when prompted.

## Initial Setup instructions

- Install [Visual Studio Code](https://code.visualstudio.com/) (if not already installed)
- Install [CMake](https://cmake.org/download/) (if not already installed)
- If on Windows, install [Visual Studio Community 2019](https://visualstudio.microsoft.com/downloads/)
- If on Mac, ensure Xcode is installed
- If on Linux, ensure gcc or clang is installed
- Open Visual Studio Code, install the following extensions:
  - [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools) - 0.26.3 or later
  - [CMake](https://marketplace.visualstudio.com/items?itemName=twxs.cmake) - 0.0.17 or later
  - [CMake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools) - 1.3.0 or later

NOTE: CMake needs to be available on your path. Test this by opening a command line and typing:

```shell
$ cmake --version
```

You should see something like:

```shell
cmake version 3.17.0-rc1

CMake suite maintained and supported by Kitware (kitware.com/cmake).
```

If you don't see this, you need to add it to your path. On Windows, reinstall and make sure you check the box that adds it to your path.

On Mac OSX, you need to do the following:

```shell
sudo nano /etc/paths
<type your password>
```

Then add 

```shell
/Applications/CMake.app/Contents/bin
```

To the end of the file, save it, and either logout and back in, or reboot.

## Testing Setup

Open the folder you previously cloned in Visual Studio Code either via the "File|Open Folder" (File|Open on Mac) option, or by doing similar to the following on the command line:
   
```shell
cd path/to/cloned-project
code .
```

Once opened in Visual Studio Code, hit Ctrl+Shift+P (Cmd+Shift+P on Mac), type "cmake configure" (without the quotes) and choose "CMake: Configure" from the menu. 

You may be asked to select a Kit. If so:

- on Windows, choose one like ```Visual Studio Community 2019 Release - amd64_x86```
- on Mac, choose "clang" (it'll probably be your only option).
- on Linux, choose GCC or clang, depending on what is shown to you

Some text should scroll along the bottom and hopefully will result in something like:

```shell
[cmake] -- Build files have been written to: /path/to/yourproject/build
```

Assuming you saw that, hit Ctrl+Shift+P (Cmd+Shift+P) again, type "cmake build" (without the quotes) and choose "CMake: Build" from the menu. 

The project should now build and the ouput you'll see will differ between Windows, Mac, Linux. 

Assuming no errors, you can then run the program in the debugger with F5 (or Ctrl+Shift+P -> CMake: Debug), or execute it directly with Ctrl+F5 (CMake: Run Without Debugging).

You can also run the built program through the command line:

```shell
$: cd /path/to/week1
$: build/week1 (build/week1.exe on windows)
Hello, World!
```
